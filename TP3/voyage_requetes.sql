--1
SELECT * from VOYAGES where VilleDepart = 'Paris';

--2
SELECT * from VOYAGES where VilleArrivee = 'Amsterdam';

--3
SELECT VilleDepart, Depart, to_char(Depart, 'HH24:MI') heure_depart from VOYAGES where VilleArrivee = 'Amsterdam';

--4
SELECT Nom from CLIENTS natural join RESERVATIONS natural join VOYAGES order by Nom,  Prix DESC;

--5
select DISTINCT Nom, VilleDepart, Code from CLIENTS natural join RESERVATIONS natural join VOYAGES where Ville = VilleDepart;

--6
--insert into Voyages values ('V000', 'Paris', 'Amsterdam',  to_date('01-08-2022-10:30','DD-MM-YYYY-HH24:MI'), to_date('07-08-2019-14:30','DD-MM-YYYY-HH24:MI'),200.00);

--7
select VilleDepart, VilleArrivee, to_char(Depart, 'DD-MM-YYYY') JDepart, to_char(Depart, 'HH24:MI') Heure from VOYAGES where (Depart-Sysdate)/30 > 3  order by Depart;

--8
select VilleDepart Ville from VOYAGES union select VilleArrivee Ville from VOYAGES;

--9
select * from CLIENTS where Ville <> 'Paris';

--10
select Nom, VilleDepart from CLIENTS natural join RESERVATIONS natural join VOYAGES where VilleDepart = 'Paris' and Ville <> 'Paris';

--11
select id, nom from CLIENTS minus  select id, nom from CLIENTS natural join RESERVATIONS natural join VOYAGES;

--12